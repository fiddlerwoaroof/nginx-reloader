To setup:

- copy the reload-nginx shell script somewhere sane:

  ```bash
  mkdir -p /root/bin
  sudo cp reload-nginx /root/bin
  ```

- copy nginx-reloader.path (change this to point to the path where the nginx configuration files are) and
  nginx-reloader.service (change this to point to the reload-nginx shell script) to /etc/systemd/system (or an
  equivalent directory see `man systemd.unit`)

  ```bash
  vim nginx-reloader.path
  vim nginx-reloader.service
  sudo cp nginx-reloader.{path,service} /etc/systemd/system
  ```

- enable the new systemd units:

  ```bash
  sudo systemctl enable nginx-reloader.{path,service}
  sudo systemctl start nginx-reloader.{path,service}
  ```

- adjust the add-reverse-proxy script to use the path in nginx-reloader.path and run the add-reverse-proxy
  script
  ```bash
  vim add-reverse-proxy
  ./add-reverse-proxy http://localhost:9203 foo.bar.baz www.foo.bar.baz
  ```

- ???

- profit.
